import os,csv
import json
import ast
import boto3
from datetime import datetime, date, timedelta
import requests
from elasticsearch import Elasticsearch
from config import MAPPER_TOKEN, COMBO_MAPPING_URL, DC_LIST_URL, ES_DELHIVERY_URL,ums_token
from utils import read_pickle, make_pickle, get_csv_record, list_s3_objects,_download_from_s3,got_csv_records
# from query import misroute_query
# from query_or import misroute_query
from pymongo import MongoClient
from config import dq_host, dq_port, dq_database, dq_collection

# creating mongo connection
client = MongoClient(dq_host, dq_port)
db_name = client[dq_database]
mis_col = db_name[dq_collection]

# get full path of directory
path = os.path.dirname(os.path.abspath(__file__))


def get_previous_week_combo():
    # extract data from mongo
    previous_week_day = mis_col.distinct('date')[-14:-7]
    cursur = mis_col.find({ 'date': {'$in': previous_week_day}, 'nsl_at_dc': True }, {'_id':0, 'nsl_at_dc':0})
    # list of remark
    ops_remark = ['AddFix not live','Delivered by Centres other than DCs/DPPs','Fake misroute of DCs']
    map_remark = ['Delivered by CpinDC not by MDC','Delivered by MSD, Not by MDC','Delivered by (MDC = MSD), Not by MDC','Delivered by RDC, Not by MDC','Loc found nearby CN','Loc found nearby MDC','Same Pin (Other issue)']
    con_remark = ['DC to DPP Misroutes','DPP to DC Misroutes','Fake misroute of DPPs','Misroute between DPPs']
    # create dictionary of repeated combo of each bucket
    pw_combo_dict = {
        'misroute': set(),
        'constellation': set(),
        'ops': set()
    }
    for row in cursur:
        if row['aseg_loc'] and row['aseg_pin']:
            if row['remark'] in ops_remark:
                pw_combo_dict['ops'].add(row['aseg_loc'] +' - '+ row['aseg_pin'])
            elif (row['remark'] in map_remark and row['cpin'] == row['aseg_pin']) or row['remark'] == 'Unmapped Loc':
                pw_combo_dict['misroute'].add(row['aseg_loc'] +' - '+ row['aseg_pin'])
            elif (row['remark'] in con_remark and row['cpin'] == row['aseg_pin']):
                pw_combo_dict['constellation'].add(row['aseg_loc'] +' - '+ row['aseg_pin'])

    return pw_combo_dict


def get_last_week_data():
    # extract data from mongo
    last_week_day = mis_col.distinct('date')[-7:]
    cursur = mis_col.find({ 'date': {'$in': last_week_day}, 'nsl_at_dc': True }, {'_id':0, 'nsl_at_dc':0})
    fetch_data = []
    fetch_keys = ['wbn','add','cpin','cty','aseg_loc','aseg_pin','cl','cn','cnc','date','live','region',\
    'misroute_nsl_count','misroute_nsl_dc','invalid_add_code','aseg_lat','aseg_long','cd','cs_sd','pdd','pdt']
    for row in cursur:
        doc = {k: row.get(k,'') for k in fetch_keys}
        fetch_data.append(doc)
    return fetch_data


def get_combo_mapping_data():

    sts_client = boto3.client('sts')
    assumedRoleObject = sts_client.assume_role(
        RoleArn="arn:aws:iam::190020191201:role/done-role-s3-for-dev-env",
            RoleSessionName="RoleSession"
            )
    credentials = assumedRoleObject['Credentials']
    s3_client = boto3.client('s3',region_name='us-east-1',
        aws_access_key_id = credentials['AccessKeyId'],
            aws_secret_access_key = credentials['SecretAccessKey'],
                aws_session_token = credentials['SessionToken']
                )

    # s3_client = boto3.client('s3') lm:b2b:fwd_2019-01-08.csv
    # folder_path = 'done-mapper/loc_full_mappings/'+ 'lm:b2c:fwd_'+str(date.today()) + '.csv'
    # folder_path = 'loc_full_mappings/'# + 'lm:b2b:fwd_2019-01-08.csv'
    # print folder_path
    # c_files_list = list_s3_objects(s3_client,'done-mapper',folder_path)
    # print c_files_list
    # c_file_path = c_file_list['Contents'][0]["Key"]
    local_path = path+'/combo_mapping_data/' + 'lm:b2b:fwd_'+str(date.today())+ '.csv'
    _download_from_s3(s3_client,'done-mapper','loc_full_mappings/lm:b2b:fwd_'+str(date.today()-timedelta(1))+ '.csv',local_path)
    # local_path = path+'/athena_data/misroute_rca_12_date.csv'
    # print local_path

    lmap_data,keys = got_csv_records(local_path)
    loc_map_data = {}
    keys = keys + ['combo']
    pin_city = {}
    for row in lmap_data:
        row['combo'] = row['loc_name']+' - '+row['loc_pin']
        if row['loc_pin'] not in pin_city:
            pin_city[row['loc_pin']] = row['city']
    for lp in lmap_data:
        combo = lp['combo']
        loc_map_data[combo] = lp

    # print pin_city
    # saving combo map data and pin city object in pickle file
    make_pickle(lmap_data, path+'/source/combo_map_data.pkl')
    make_pickle(pin_city, path+'/source/pin_city.pkl')

    # # read data from pickle
    # loc_map_data = read_pickle(path+'/source/combo_map_data.pkl')
    # pin_city = read_pickle(path+'/source/pin_city.pkl')
    # print loc_map_data
    return loc_map_data, pin_city


def get_cpin_mapping_data():
    # copy latest hq pin mapping file from folder
    os.system('cp /home/ubuntu/data/live_projects/misroute_analysis_report/source/hq_pin_mapping.csv '+path+'/source/')
    # read hq_pin mapping file
    hq_pin_map_data = get_csv_record(path+'/source/hq_pin_mapping.csv') #manually uplaod in folder
    # make pin to dc mapping dict
    pin_dc_details = {hq_dc['pin']: hq_dc['dispatch_center'].split(" (")[0] for hq_dc in hq_pin_map_data}

    return pin_dc_details


def get_dc_data():
    # get updated dc list
    full_dc_name_dict = read_pickle(path+'/source/full_dc_name_dict.pkl')
    full_dc_id_dict = read_pickle(path+'/source/full_dc_id_dict.pkl')
 
    final_dc_name_dict = {}
    dc_name_dict = {}
    # print dc_name_dict
    dc_id_dict = {}
    mapper_response = requests.get(DC_LIST_URL, headers={'Authorization': ums_token})
    json_dc_data = mapper_response.json()
    # print json_dc_data
    # make active dc_name and dc_id mapping

    # saving dc name dict and dc id dict object in pickle file
    for k, v in json_dc_data.iteritems():
      if isinstance(v, dict):
          for k, v in v.iteritems():
              if isinstance(v, list):
                  dc_name_dict = {jd['name']: [jd['property_lat'],jd['property_long']] for jd in v}
                  dc_id_dict = {jd['facility_code']: jd['name'] for jd in v}
    
    # dc_name_dict = read_pickle(path+'/source/dc_name_dict.pkl')
    # dc_id_dict = read_pickle(path+'/source/dc_id_dict.pkl')

    final_dc_name_dict = full_dc_name_dict.copy()
    final_dc_name_dict.update(dc_name_dict)
   

    final_dc_id_dict = full_dc_id_dict.copy()
    final_dc_id_dict.update(dc_id_dict)
    # print final_dc_id_dict

    make_pickle(final_dc_name_dict, path+'/source/dc_name_dict.pkl')
    make_pickle(final_dc_id_dict, path+'/source/dc_id_dict.pkl')
    
    # # # # read data from pickle
    # dc_name_dict = read_pickle(path+'/source/dc_name_dict.pkl')
    # dc_id_dict = read_pickle(path+'/source/dc_id_dict.pkl')
    return final_dc_name_dict, final_dc_id_dict

def get_misroute_data():
    sts_client = boto3.client('sts')
    assumedRoleObject = sts_client.assume_role(
        RoleArn="arn:aws:iam::190020191201:role/done-role-s3-for-dev-env",
            RoleSessionName="RoleSession"
            )
    credentials = assumedRoleObject['Credentials']
    s3_client = boto3.client('s3',region_name='us-east-1',
        aws_access_key_id = credentials['AccessKeyId'],
            aws_secret_access_key = credentials['SecretAccessKey'],
                aws_session_token = credentials['SessionToken']
                )
    # s3_client = boto3.client('s3')
    folder_path = 'b2c-delivered-data/misroute_data/'+ str(date.today()) + '/'
    folder_path1 = 'b2c-delivered-data/del_count/'+ str(date.today()) + '/'

    #Misroute_data_raw
    files_list = list_s3_objects(s3_client,'done-athena-results',folder_path)
    file_path = files_list['Contents'][0]["Key"]
    for name in files_list['Contents']:
        if (name["Key"].find("metadata")==-1 and name["Key"].find("count")==-1):
            file_path = name["Key"]
            # print(file_path)
    local_path = path+'/athena_data/misroute_rca_' + str(date.today())+ '.csv'
    _download_from_s3(s3_client,'done-athena-results',file_path,local_path)

    #Total_delivered_count
    d_files_list = list_s3_objects(s3_client,'done-athena-results',folder_path1)
    d_file_path = d_files_list['Contents'][0]["Key"]
    for name in d_files_list['Contents']:
        if (name["Key"].find("metadata")==-1 and name["Key"].find("count")==-1):
            d_file_path = name["Key"]
    d_local_path = path+'/athena_data/b2b_delivered_' + str(date.today())+ '.csv'
    # # print d_local_path
    _download_from_s3(s3_client,'done-athena-results',d_file_path,d_local_path)


    def get_csv_records(file_name):
        data = open(file_name,'rU')
        reader = csv.DictReader(data)
        records_list = []
        keys = reader.next()
        for row in reader:
           # record_dict = dict(zip(keys, row))
            records_list.append(row)
        data.close()
        return records_list 

    new_data = []

    athena_data = get_csv_records(local_path)
    for row in athena_data:
        row['cn'] = row['cn'].split(" (")[0]
        new_data.append(row)

    final_scroll_data = []
    for each in new_data:
        # print each
        # print type(each)
        # break
        final_scroll_data.append(
            {
            '_id': each['wbn'], 
            '_source':{
                'nsl' : [{'code':each['cs_nsl'],'slid':each['cs_slid']}],
                'cpin':each['cpin'],
                'mwn':each['mwn'],
                'add':each['add'],
                'cnc':each['cnc'],
                'rgn':each['rgn'],
                'cd':each['cd'],
                'cn': each['cn'],
                'cl':each['cl'],
                'pdd':each['pdd'],
                'cty':each['cty'],
                'cs':{'sd':each['cs_sd']},
                'aseg':{'lat':each['aseg_lat'],
                        'lon':each['aseg_lon'],
                        'loc':each['aseg_loc'],
                        'pin':each['aseg_pin'],
                        'invalid_add_code':each['aseg_invalid_add_code']
                        }}
                })
    # total count of documents
    print len(final_scroll_data)   #list of dictionary

    # saving athena misroute data object in pickle file    
    make_pickle(final_scroll_data, path+'/source/athena_misroute_data.pkl')

    print("misroute_count_completed")
    return final_scroll_data

# get_dc_data()# 
# get_misroute_data()
# get_combo_mapping_data()
# get_dc_data()