b2c_misrouted_query = """
with tmp as
(
select wbn,mwn
      ,cd + interval '330' minute as creation_date
      ,cs_sd
      ,concat( coalesce(add_0,''),', ',coalesce(add_1,''),', ',coalesce(add_2,''),', ',coalesce(add_3,'') ) as add
      ,cpin, cty,aseg_loc, aseg_pin, aseg_lat, aseg_lon
      ,cl,cn, cnc, aseg_invalid_add_code,cs_nsl,cs_slid,pdd,rgn,cd
from
(
select wbn,cs_ss,mwn,
  add_0,add_1,add_2,add_3,
  cs_nsl,cs_slid,aseg_loc,aseg_pin,aseg_lat,aseg_lon,aseg_invalid_add_code,cl,cn,cnc,cpin,cty,rgn,cd,cs_sd,pdd,
  row_number() over (partition by wbn order by cs_sd desc) as row
  from express_dwh_3m.package_s3_parquet_3m
  where ad >= date_format(date_trunc('day',current_date - interval '1' day) - interval '00' minute, '%Y-%m-%d-%H')
  and ad <= date_format(date_trunc('day',current_date - interval '0' day) - interval '00' minute, '%Y-%m-%d-%H')
  and cs_sd >= date_trunc('day',current_timestamp - interval '1' day) - interval '330' minute
  and cs_sd < date_trunc('day',current_timestamp - interval '0' day) - interval '330' minute
  and pdt in ('B2B','Heavy')
  and NOT(regexp_like(lower(cl),'skynet|delhivery'))
) where row = 1
and cs_ss = 'Delivered'
)

select nsl_data.wbn as wbn,mwn,nsl_data.cs_nsl as cs_nsl,nsl_data.cs_slid as cs_slid,cl,cn,cnc,add,cpin,cty,rgn,
            aseg_loc,aseg_pin,aseg_lat,aseg_lon,aseg_invalid_add_code,
            cd,cs_sd,pdd
from
(
select wbn,cs_nsl,cs_slid
from
(
   select wbn,cs_nsl,cs_slid,
   row_number() over (partition by wbn order by cs_sd desc) as row
   from express_dwh_3m.package_s3_parquet_3m
   where ad >= date_format((select min("creation_date") from tmp), '%Y-%m-%d-%H')
   and cs_nsl in ('DTUP-207','DTUP-216','DLYMR-118')
   and wbn in (select wbn from tmp)
) where row=1
) as nsl_data
join tmp on
tmp.wbn = nsl_data.wbn

"""

b2c_delivered_query = """
select count(wbn) as wbn_count
from
(
select wbn,cs_ss,
  row_number() over (partition by wbn order by cs_sd desc) as row
  from express_dwh_3m.package_s3_parquet_3m
  where ad >= date_format(date_trunc('day',current_date - interval '1' day) - interval '00' minute, '%Y-%m-%d-%H')
  and ad <= date_format(date_trunc('day',current_date - interval '0' day) - interval '00' minute, '%Y-%m-%d-%H')
  and cs_sd >= date_trunc('day',current_timestamp - interval '1' day) - interval '330' minute
  and cs_sd < date_trunc('day',current_timestamp - interval '0' day) - interval '330' minute
  and pdt in ('B2B','Heavy')
  and NOT(regexp_like(lower(cl),'skynet|delhivery'))
) where row = 1
and cs_ss = 'Delivered'
""" 
