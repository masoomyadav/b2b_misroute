import os, sys, time
import boto3
from datetime import datetime, date, timedelta
from query_misroute import b2c_misrouted_query,b2c_delivered_query

path = os.path.dirname(os.path.abspath(__file__))

# from misroute_query_rca import b2c_misrouted_query
def run_query(client, query, database, s3_output):
    response = client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': s3_output,
        }
    )
    # print('Execution ID: ' + response['QueryExecutionId'])
    return response

# Athena function for check status of athena queries using Execution_Id
def get_query_status(client, Execution_Id):
    response = client.get_query_execution(
        QueryExecutionId=Execution_Id
    )
    query_id = response['QueryExecution']['QueryExecutionId']
    status = response['QueryExecution']['Status']['State']
    print('Started')
    print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    while status not in ['SUCCEEDED', 'FAILED', 'CANCELLED']:
        # print('|' + status, end='')
        time.sleep(30)
        response = client.get_query_execution(
            QueryExecutionId=Execution_Id
        )
        status = response['QueryExecution']['Status']['State']
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    if status in ['SUCCEEDED']:
        return True
    if status in ['FAILED', 'CANCELLED']:
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))
        print("Failure Reason: {}".format(
            response['QueryExecution']['Status'].get('StateChangeReason', 'Error Unknown')))
        return False
    else:
        return False


# Athena Function to run query on Athena and fetch data
def run_query_to_athena(query):

        # Athena configuration
    sts_client = boto3.client('sts')
    assumedRoleObject = sts_client.assume_role(
        RoleArn="arn:aws:iam::190020191201:role/stsDataServices",
            RoleSessionName="RoleSession"
            )
    credentials = assumedRoleObject['Credentials']
    client = boto3.client('athena',region_name='us-east-1',
        aws_access_key_id = credentials['AccessKeyId'],
            aws_secret_access_key = credentials['SecretAccessKey'],
                aws_session_token = credentials['SessionToken']
                )
    date_today = str(date.today())
    s3_output = "s3://done-athena-results/b2c-delivered-data/misroute_data/" + date_today +"/"
    database = 'express_dwh_3m'
    table = 'package_s3_parquet_3m'

    # print("Executing query: %s" % (q))
    res = run_query(client, query, database, s3_output)

    if res['QueryExecutionId']:
        print("Query Successful. Downloading data: ")
    # date_file_name = str(date.today())
    # if get_query_status(client, q_id):
    #     print("Query Successful. Downloading data: ")
    #     # os.system("aws s3 cp " + s3_output + date_file_name + ".csv  {}".format(output_filename))
    #     return True
    # else:
    #     print("Query Unsuccessful. Please check the query or the Athena server status")
    #     return False

    # format query
    # print (athena_query)


athena_query = b2c_misrouted_query
#athena_out_file = path + '/athena_data/'+'athena_misroute_data' + str(date.today())+'.csv'
run_query_to_athena(athena_query)

def delivered_query_athena(query):
    sts_client = boto3.client('sts')
    assumedRoleObject = sts_client.assume_role(
        RoleArn="arn:aws:iam::190020191201:role/stsDataServices",
            RoleSessionName="RoleSession"
            )
    credentials = assumedRoleObject['Credentials']
    client = boto3.client('athena',region_name='us-east-1',
        aws_access_key_id = credentials['AccessKeyId'],
            aws_secret_access_key = credentials['SecretAccessKey'],
                aws_session_token = credentials['SessionToken']
                )
    date_today = str(date.today())
    s3_output = "s3://done-athena-results/b2c-delivered-data/del_count/" + date_today + "/"
    database = 'express_dwh_3m'
    table = 'package_s3_parquet_3m'

    # print("Executing query: %s" % (q))
    res = run_query(client, query, database, s3_output)

    if res['QueryExecutionId']:
        print("Query Successful. Downloading data: ")

athena_delivered_query = b2c_delivered_query
delivered_query_athena(athena_delivered_query)