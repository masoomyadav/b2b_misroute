import os, csv
import sys
import pandas as pd
from datetime import date,timedelta
from send_mail_boto import mail_send

    
path = os.path.dirname(os.path.realpath(__file__))

file_name = path+'/files/misroute_rca_b2b_'+str(date.today())+'.csv'
# file_name = path+'/files/misroute_rca_b2b_2019-03-14.csv'

def b2b_summary(file_name):
    df = pd.read_csv(file_name)
    row_count = df.shape[0]
    gdf = df.groupby(['broad_cat','remark','team'], as_index = False).agg({'wbn': 'count'}).sort_values(['wbn'], ascending=False)
    gdf['percentage'] = (100*gdf.wbn/gdf.wbn.sum()).round(2).astype(str) + '%'
    gdf = gdf.rename(columns={'broad_cat': 'Broad_category','remark': 'Misroute Remark', 'team': 'Concerned Team', 'wbn': 'Count of Waybill', 'percentage': 'Percentage'})
    other_html = gdf[gdf['Concerned Team'].str.match('AddFix')==False].to_html(index=False)
    addfix_not_live_html = gdf[gdf['Concerned Team']=='AddFix_Not_Live'].to_html(index=False) # addfix not live
    addfix_html = gdf[gdf['Concerned Team']=='AddFix Team'].to_html(index=False)# addfix live
    # print addfix_not_live_html
    # print df[df['team']=='Not_Live'].shape[0]
    return other_html, df[df['team']=='OPS'].shape[0], df[df['team']=='Locality Mapping Team'].shape[0], df[df['team']=='Constellation Team'].shape[0], addfix_not_live_html, df[df['team']=='AddFix_Not_Live'].shape[0], addfix_html, df[df['team']=='AddFix Team'].shape[0], df.shape[0]
# b2b_summary(file_name)
def get_csv_records(file_name):
        data = open(file_name,'rU')
        reader = csv.DictReader(data)
        records_list = []
        # keys = reader.next()
        for row in reader:
           # record_dict = dict(zip(keys, row))
            records_list.append(row)
        data.close()
        return records_list

if __name__ == '__main__':
    
    os.system("python mis.py 1")
    # b2b_summary(file_name)

    d_local_path = path+'/athena_data/b2b_delivered_' + str(date.today())+ '.csv'
    # print d_local_path
    # d_local_path = path+'/athena_data/645349.csv'
    athena_delivered_data = get_csv_records(d_local_path)
    delivered_count = athena_delivered_data[0]["wbn_count"]

    other_html, ops_count, lmt_count, cnst_count, addfix_not_live_html, af_not_count, addfix_html, af_count, row_count = b2b_summary(file_name)
    total_count = lmt_count+ops_count+cnst_count

#     # cwd = os.getcwd()
#     # os.chdir(path+'/files/')
#     # os.system('zip '+zip_file+' '+file_name.split('/')[-1])
#     # os.chdir(cwd)

    # get summary to html
    misroute_percentage = round(((int(row_count)/float(delivered_count))*100),1)
    # os.remove(path+'/athena_data/misroute_rca_' + str(date.today())+ '_delivered.csv')


    # check today's reports are create sucessfuly or not , if not send a mail to user
    if (not os.path.isfile(file_name)) or (row_count < 10):
        to = "monika.yadav@delhivery.com, masoom.yadav@delhivery.com"
        # to = "rahul.verma4@delhivery.com, soniya.mehar@delhivery.com, kapil.garg@delhivery.com"
        text = "Something wrong in misroute report file is not created properly. the no of row present in file is "+str(row_count)
        mail_send("noreply@delhivery.com", to, "Report Failed :: Misroute B2B Report "+str(date.today()), text, [file_name])
        sys.exit()

    to = "monika.yadav@delhivery.com, masoom.yadav@delhivery.com, gursheen.chawla@delhivery.com, kapil.garg@delhivery.com, manish.pandey@delhivery.com"
    # to = "masoom.yadav@delhivery.com"
    # to = "monika.yadav@delhivery.com, masoom.yadav@delhivery.com"


    subject = "Misroute B2B RCA Report"

    html = """\
    <html>
    <head></head>
    <body>
    <p>Hi,<br>
    <br>
    Please find the summary of Misroute Data. <br>
    <br>
    MDC --- Mapped Facility <br>
    MSD --- Maximum Serving Facility <br>
    RDC --- Recommended Facility <br>
    CN --- Delivered Facility <br>
    <br>
    <br>
    <strong>A. Total Delivered Count --- </strong>{}<br>
    <strong>B. Total Misroute Count --- </strong>{}<br>
    <strong>C. Misroute Percentage --- </strong>{}%
    <br>
    <br>
    1)  <strong>OPS, Constellation, Locality Mapping Teams Misroutes:</strong> (WBN Count -- {}) <br>
    <br>
    {}
    <br>
    <br>
    2)  <strong>AddFix Team Misroutes:</strong> (WBN Count -- {}) <br>
    <br>
    {}
    <br>
    <br>
    3)  <strong> Addfix Not Live:</strong> (WBN Count -- {}) <br>
    <br>
    <br>{}<br>
    </p>
    </body>
    </html>
    """.format(delivered_count,row_count, misroute_percentage, total_count, other_html, af_count, addfix_html, af_not_count, addfix_not_live_html)
            # other_html, ops_count, lmt_count,addfix_not_live_html, af_not_count, addfix_html, af_count, row_count

    # # save html into file
    # fl = open('mail_body.html', 'w')
    # fl.write(html)
    # fl.close()

    # send mail
    mail_send("noreply@delhivery.com", to, subject, html, [file_name], True)