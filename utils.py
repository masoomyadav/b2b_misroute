import csv
import pickle
import boto3
import time
import sys
reload(sys)
sys.setdefaultencoding('utf8')

def make_pickle(file_data, file_name):
    """
    to make pickle file from python objects
    """
    with open(file_name, 'wb') as mp:
        pickle.dump(file_data, mp)


def read_pickle(file_name):
    """
    to read pickle file for python objects
    """ 
    with open(file_name, 'rb') as mp:
        data = pickle.load(mp)
    return data


def get_csv_record(file_name):
    """
    to read csv file and return its data in list of dict format
    """     
    return list(csv.DictReader(open(file_name, 'r')))


def write_csv(file_data, file_keys, file_name):
    """
    write list of dict format data in csv file with given keys and given path
    """
    write = csv.DictWriter(open(file_name, 'w'), file_keys)
    write.writeheader()
    for data in file_data:
        write.writerow(data)



#================================== Connection Class ==================================================================

class cls_connection(object):
    """
    All the static connections will be initialized with this class.
    """

    def __init__(self):
        self.S3_CLIENT = None
        self.ATHENA_CLIENT = None


    def get_athena(self):
        """
        Get the boto3 athena connection
        """
        if not self.ATHENA_CLIENT:
            try:
                logger.info("Getting the connection to AWS athena")
                self.ATHENA_CLIENT = boto3.client('athena', region_name=_access_env_vars("athena_region"))
            except Exception as e:
                logger.error("Error occured while connecting to athena : %s", e)
                raise e
        return self.ATHENA_CLIENT


CLIENT = cls_connection()

#============================================== S3 Functions ============================================

# def _download_from_s3(s3_client, bucket, key,local_path):
#     """
#     Downloads data from s3 bucket
#     """
#     try:
#         logger.info("Downloading from bucket %s using key %s",bucket, key)
#         s3_client.download_file(bucket, key, local_path)
#     except Exception as err:
#         logger.error("Error occured while downloading from S3 bucket: %s key: %s as:: %s",bucket, key, err)


def _get_from_s3(s3_client, bucket, key):
    """
    Reads data from s3 bucket
    """
    # new_data = []
    data = s3_client.get_object(Bucket=bucket, Key=key)
    # print data['Body']
    if 'Body' in data:
        # logger.info("Data found in bucket %s using key %s",bucket, key)
        # new_data.append(data['Body'].read())
        return data['Body'].read()
    else:
        # print("No data found for bucket %s using key %s",bucket, key)
        return None
    # return new_data


def list_s3_objects(s3_client, bucket, key):
    data = s3_client.list_objects(Bucket=bucket, Prefix=key)
    if data: 
        return data
    else:
        return None

def _download_from_s3(s3_client, bucket, key,local_path):
# Downloads data from s3 bucket
    try:
        # print key
        s3_client.download_file(bucket, key, local_path)
    except Exception as err:
        print("Error occured while downloading from S3 bucket",err)


def got_csv_records(file_name):
   
   data = open(file_name,'rU')
   reader = csv.reader(data)
   records_list = []
   keys = reader.next()
   for row in reader:
       record_dict = dict(zip(keys, row))
       records_list.append(record_dict)
   data.close()
   return records_list,keys

