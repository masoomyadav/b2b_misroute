from datetime import datetime, date, timedelta
date_today = str(date.today()-timedelta(1))
# elastic search credentials url
ES_DELHIVERY_URL = "https://shikhar.khattar:u2b76isw8rsr95ebbn@express-elastic.delhivery.com"

# misroute rca google doc url
MISROUTE_RCA_DOC_URL = "https://docs.google.com/document/d/1E6M4P8IvQJFYnv9edptRw3tdcwIuyTjjxFJpAiI9hD0/"

# senior manager mapping sheet url
SM_MAPPING_URL = "https://docs.google.com/spreadsheets/d/1GGzHev622jQ5_fTYPGStNPRVJNLz4VMOB6NnVn9gBDI/"

# delhivery mapper token
MAPPER_TOKEN = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1hc29vbS55YWRhdiIsInBob25lX251bWJlciI6Iis5MTk3MTExMjk4OTUiLCJsYXN0X25hbWUiOiJZYWRhdiIsInBpbiI6ZmFsc2UsInJlYWRfYWxsX2NlbnRlcnMiOnRydWUsInVzZXJfdHlwZSI6Ik9SIiwiaWF0IjoxNTUxMDY2ODM4LCJhdWQiOiIuZGVsaGl2ZXJ5LmNvbSIsImZpcnN0X25hbWUiOiJNYXNvb20iLCJzdWIiOiJ1bXM6OnVzZXI6OjQxMTRmYjgyLTg2MWItNDU4Zi05ZjA3LWQzOTEzMjhkNTc5MSIsIndyaXRlX2FsbF9jZW50ZXJzIjpmYWxzZSwiaWRsZSI6MTU1MTY3MTYzOCwiZXhwIjoxNTUxMjQzMjM4LCJkYXJ3aW5fZmlkIjoiSU5EMTIyMDAzQUFCIiwidG9rZW5faWQiOiIxN2UwOWJkMC0zYjY1LTRjYzMtYWE0Yy0wYjU1YmM0MWZkNTYiLCJlbWFpbCI6Im1hc29vbS55YWRhdkBkZWxoaXZlcnkuY29tIiwiYXBpX3ZlcnNpb24iOiJ2MiJ9.3hsDtppsTISh5uZKBkGYm7GRy2eKw5hSG6hKfCKQMBE'

# ums_token = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1hc29vbS55YWRhdiIsInBob25lX251bWJlciI6Iis5MTk3MTExMjk4OTUiLCJsYXN0X25hbWUiOiJZYWRhdiIsInBpbiI6ZmFsc2UsInJlYWRfYWxsX2NlbnRlcnMiOnRydWUsInVzZXJfdHlwZSI6Ik9SIiwiaWF0IjoxNTUxNzcxNTA0LCJhdWQiOiIuZGVsaGl2ZXJ5LmNvbSIsImZpcnN0X25hbWUiOiJNYXNvb20iLCJzdWIiOiJ1bXM6OnVzZXI6OjQxMTRmYjgyLTg2MWItNDU4Zi05ZjA3LWQzOTEzMjhkNTc5MSIsIndyaXRlX2FsbF9jZW50ZXJzIjpmYWxzZSwiaWRsZSI6MTU1MjM3NjMwNCwiZXhwIjoxNTUxOTQ3OTA0LCJkYXJ3aW5fZmlkIjoiSU5EMTIyMDAzQUFCIiwidG9rZW5faWQiOiI2ZjdkYjc3ZS03YzViLTRmM2UtYjUyYS0zOTk5OWJkOTVjNTciLCJlbWFpbCI6Im1hc29vbS55YWRhdkBkZWxoaXZlcnkuY29tIiwiYXBpX3ZlcnNpb24iOiJ2MSJ9.kcAg8NgKMPtHJ5yX63iD7ITrZvav_JxZ6B_oBNtuFik'
ums_token = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImdldC1mYWNpbGl0eS1saXN0LW1pc3JvdXRlLXJjYS1kcSIsInRva2VuX25hbWUiOiJnZXQtZmFjaWxpdHktbGlzdC1taXNyb3V0ZS1yY2EtZHEiLCJjZW50ZXIiOlsiSU5EMTIyMDAzQUFCIl0sInVzZXJfdHlwZSI6Ik5GIiwiYXBwX2lkIjozMCwiYXVkIjoiLmRlbGhpdmVyeS5jb20iLCJmaXJzdF9uYW1lIjoiZ2V0LWZhY2lsaXR5LWxpc3QtbWlzcm91dGUtcmNhLWRxIiwic3ViIjoidW1zOjp1c2VyOjo0YjJhYjNjMC00NjQ5LTExZTktODA1Ni0wNmI2MmZjZTQ2NjIiLCJleHAiOjE4Njc5MjE2MjIsImFwcF9uYW1lIjoiRkFBUyIsImFwaV92ZXJzaW9uIjoidjIifQ.zRFwhlAq0fvAZGvzFH-_HGoDEG1JefT3l4VtAGR7r1g'
# combo mapping url
COMBO_MAPPING_URL = "https://api-mapper.delhivery.com/core/api/v2/full_mapping/?product_type=b2c&service_type=lm&flow=fwd"

# active dc list url
# DC_LIST_URL = "https://api-mapper.delhivery.com/core/api/v2/fac_service/?service_type=lm&flow=fwd&product_type=b2c"
# DC_LIST_URL = "https://faas-api.delhivery.com/v2/facilities/?fields=property_lat,property_long&page=0"
DC_LIST_URL = "https://faas-api.delhivery.com/v2/facilities/?fields=property_lat,property_long&updated_at="+date_today+"&size=200"
# dq mongo auth
dq_host = '30.0.6.15'
dq_port = 27017
dq_database = 'rca'
dq_collection = 'misroute_b2b_bucket'
