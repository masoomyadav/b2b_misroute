import os
from datetime import date, timedelta,datetime
from aerial_catalog import aerial, aerial_to_road
from utils import write_csv
from data import get_misroute_data, get_combo_mapping_data, \
get_cpin_mapping_data, get_dc_data, mis_col

# get full path of directory
path = os.path.dirname(os.path.abspath(__file__))

# get v3 pins data
# v3_pins = get_live_pins()

# get combo mapping data and pin city mapping
loc_map_dict, pin_city = get_combo_mapping_data()

# get hq_pin and combo mapping data
pin_map_dict = get_cpin_mapping_data()

# dispatch center dicts
dc_name_dict, dc_id_dict = get_dc_data()


def get_misroute_remark(row):
	"""
	this function is used to identify misroute remarks
	"""

   
	# distance flag
	dist_flag = row['loc2cn_dist'] and row['loc2map_dist']

	# initial value for remark and team
	broad_cat,remark, team = '','', ''

	# if not row['live']:
		# remark, team = 'AddFix not live', 'OPS'
	# addfix not live cases
	if row['live'] == 'no' and not row['aseg_loc']:
		broad_cat, remark, team = 'Delivered by Facility other than Cpin Facility','AddFix Not Live', 'AddFix_Not_Live'   #AddFix_Not_Live
		# else:
		# 	remark, team = 'AddFix Not Live', 'OPS'

	# unmapped locality
	elif (row['aseg_loc']) and (not row['combo_map_dc']):
		broad_cat, remark, team = 'Unmapped Locality', 'Unmapped Loc', 'OPS'     #changed according to b2b report

	# not delivered by dispatched center
	elif (row['cn'] not in dc_name_dict.keys()):
		broad_cat, remark, team = 'Delivered by Facility other than Mapped Facility', 'Delivered by Centres other than DCs/DPPs', 'OPS'


	# elif (not row['aseg_loc']) and (row['mapped_dc'] == row['cn']):
	#     remark, team = 'Fake Misrouted in Loc Not Found', 'OPS'

	# to identify fake misroute where mapped dc and delivered dc is same
	elif row['mapped_dc'] == row['cn']:
		# print row['cs_sd']
		#'last_update_datetime': '2017-09-11 06:03:30.528185+00:00'
		if row['cd'] and row['last_update_datetime'] and row['cs_sd'] and datetime.strptime(row['cd'].split(".")[0],"%Y-%m-%d %H:%M:%S") < datetime.strptime(row['last_update_datetime'].split("+")[0].split(".")[0],"%Y-%m-%d %H:%M:%S") < datetime.strptime(row['cs_sd'].split(".")[0],"%Y-%m-%d %H:%M:%S"):
			broad_cat, remark, team = 'None','Mapping corrected', 'None'

		# loc not found fake misroutes
		elif (not row['aseg_loc']):
			broad_cat, remark, team = 'Delivered by Facility other than Mapped Facility', 'Fake Misrouted in Loc Not Found', 'OPS'

		elif ('dpp' in row['cn'].lower()):
			broad_cat, remark, team = 'Constellation Issue', 'Fake misroute of DPPs', 'Constellation Team'
		else:
			broad_cat, remark, team = 'Delivered by Facility other than Mapped Facility', 'Fake misroute of DCs', 'OPS'

	# loc not found by addfix
	elif (not row['aseg_loc']) and row['live'] == 'yes':
		broad_cat, remark, team = 'Addfix Issue', 'Loc not found', 'AddFix Team'
	# elif (not row['aseg_loc']) and row['live'] == 'v1':
		# remark, team = 'Loc not found-V1', 'AddFix Team'

	# to identify dpp misroute
	elif 'dpp' in row['mapped_dc'].lower() and 'dpp' not in row['cn'].lower():
		broad_cat, remark, team = 'Constellation Issue', 'DPP to DC Misroutes', 'Constellation Team'
	elif 'dpp' not in row['mapped_dc'].lower() and 'dpp' in row['cn'].lower():
		broad_cat, remark, team = 'Constellation Issue', 'DC to DPP Misroutes', 'Constellation Team'
	elif 'dpp' in row['mapped_dc'].lower() and 'dpp' in row['cn'].lower():
		broad_cat, remark, team = 'Constellation Issue', 'Misroute between DPPs', 'Constellation Team'

	# to identify pin change cases
	elif (row['cpin'] != row['aseg_pin']):
		# inter city cases
		if pin_city.get(row['cpin'], '') != pin_city.get(row['aseg_pin'], ''):
			broad_cat, remark, team = 'Addfix Issue', 'Pin Change (Inter City)', 'AddFix Team'
		# (cpin == aseg_pin) and (cpin_dc != combo_dc) and (cn == cpin_dc)
		elif (row['cpin_map_dc'] != row['combo_map_dc']) and (row['cpin_map_dc'] == row['cn']):
			broad_cat, remark, team = 'Addfix Issue', 'Delivered by Cpin Facility Not by Mapped Facility', 'AddFix Team'
		# recommended_dc == maximum_serving_dc == cn and loc2cn_dist < loc2map_dist
		elif dist_flag and (row['recommended_dc'] == row['maximum_serving_dc'] == row['cn']) and (row['loc2cn_dist'] < row['loc2map_dist']):
			broad_cat, remark, team = 'Addfix Issue', 'Delivered by (RDC = MSD), Not by Mapped Facility', 'AddFix Team'
		# maximum_serving_dc == cn and loc2cn_dist < loc2map_dist
		elif dist_flag and (row['maximum_serving_dc'] == row['cn']) and (row['loc2cn_dist'] < row['loc2map_dist']):
			broad_cat, remark, team = 'Addfix Issue', 'Delivered by MSD, Not by Mapped Facility', 'AddFix Team'
		# recommended_dc == cn and loc2cn_dist < loc2map_dist
		elif dist_flag and (row['recommended_dc'] == row['cn']) and (row['loc2cn_dist'] < row['loc2map_dist']):
			broad_cat, remark, team = 'Addfix Issue', 'Delivered by RDC, Not by Mapped Facility', 'AddFix Team'
		# (loc2map_dist < 40) and (loc2cn_dist - loc2map_dist) <= 0
		elif dist_flag and ( row['loc2map_dist'] < 40 and (row['loc2cn_dist'] - row['loc2map_dist']) <= 0 ):
			broad_cat, remark, team = 'Addfix Issue', 'Loc Found nearby CN', 'AddFix Team'
		# (loc2map_dist < 40) and (loc2cn_dist - loc2map_dist) > 0
		elif dist_flag and ( row['loc2map_dist'] < 40 and (row['loc2cn_dist'] - row['loc2map_dist']) > 0 ):
			broad_cat, remark, team = 'Addfix Issue', 'Loc Found nearby Mapped Facility', 'AddFix Team'
		# pin change other cases
		else:
			broad_cat, remark, team = 'Addfix Issue', 'Pin Change (Other issue)', 'AddFix Team'

	# to identify same pincode case
	else:
		# (cpin == aseg_pin) and (cpin_dc != combo_dc) and (cn == cpin_dc)
		if (row['cpin_map_dc'] != row['combo_map_dc']) and (row['cpin_map_dc'] == row['cn']):
			broad_cat, remark, team = 'Delivered by Facility other than Mapped Facility', 'Delivered by Cpin Facility Not by Mapped Facility', 'OPS'
		# recommended_dc == maximum_serving_dc == cn and loc2cn_dist < loc2map_dist
		elif dist_flag and (row['recommended_dc'] == row['maximum_serving_dc'] == row['cn']) and (row['loc2cn_dist'] < row['loc2map_dist']):
			broad_cat, remark, team = 'Delivered by Facility other than Mapped Facility', 'Delivered by (RDC = MSD), Not by Mapped Facility', 'OPS'
		# maximum_serving_dc == cn and loc2cn_dist < loc2map_dist
		elif dist_flag and (row['maximum_serving_dc'] == row['cn']) and (row['loc2cn_dist'] < row['loc2map_dist']):
			broad_cat, remark, team = 'Delivered by Facility other than Mapped Facility', 'Delivered by MSD, Not by Mapped Facility', 'OPS'
		# recommended_dc == cn and loc2cn_dist < loc2map_dist
		elif dist_flag and (row['recommended_dc'] == row['cn']) and (row['loc2cn_dist'] < row['loc2map_dist']):
			broad_cat, remark, team = 'Delivered by Facility other than Mapped Facility', 'Delivered by RDC, Not by Mapped Facility', 'OPS'       #all 4 above need to chnage team
		# (loc2map_dist < 40) and (loc2cn_dist - loc2map_dist) < 0
		elif dist_flag and ( row['loc2map_dist'] < 40 and (row['loc2cn_dist'] - row['loc2map_dist']) <= 0 ):
			broad_cat, remark, team = 'Delivered by Facility other than Mapped Facility', 'Loc Found nearby CN', 'Locality Mapping Team'
		# (loc2map_dist < 40) and 0 < (loc2cn_dist - loc2map_dist) < 50
		elif dist_flag and ( row['loc2map_dist'] < 40 and 0 < (row['loc2cn_dist'] - row['loc2map_dist']) < 50 ):
			broad_cat, remark, team = 'Delivered by Facility other than Mapped Facility', 'Loc Found nearby Mapped Facility', 'Locality Mapping Team'
		# (loc2map_dist < 40) and (loc2cn_dist - loc2map_dist) >= 50
		elif dist_flag and ( row['loc2map_dist'] < 40 and (row['loc2cn_dist'] - row['loc2map_dist']) >= 50 ):
			broad_cat, remark, team = 'Addfix Issue', 'Loc Found nearby Mapped Facility', 'AddFix Team'
		# same pin other issue
		else:
			broad_cat, remark, team = 'Delivered by Facility other than Mapped Facility', 'Same Pin (Other issue)', 'Locality Mapping Team'

	return broad_cat, remark, team


def get_map_dc(row):
	"""
	input:: row (dict) -> it contains all required fields from ES document.
	return:: cpin_map_dc, combo_map_dc, mapped_dc
	"""

	loc_name = row['_source'].get('aseg',{}).get('loc', '')
	pin_name = str(row['_source'].get('aseg',{}).get('pin', ''))[:6]
	# get combo and cpin
	combo = loc_name+' - '+pin_name if loc_name and pin_name else ''
	cpin = str(row['_source']['cpin'])
	# get dcs
	combo_info = loc_map_dict.get(combo, {})
	# print combo_info
	cpin_map_dc = pin_map_dict.get(cpin, '')
	# print cpin_map_dc
	combo_map_dc = combo_info.get('existing_facility', '')
	# print combo_map_dc
	recommended_dc = combo_info.get('recommended_facility', '')
	# print recommended_dc
	maximum_serving_dc = combo_info.get('maximum_serving_facility', '')
	last_update_datetime = combo_info.get('last_update_datetime','')
	# mapped_dc
	if combo:
		mapped_dc = combo_map_dc
		if not mapped_dc:
			mapped_dc = pin_map_dict.get(combo[-6:], '')
	else:
		mapped_dc = cpin_map_dc

	return cpin_map_dc, combo_map_dc, mapped_dc, recommended_dc, maximum_serving_dc, last_update_datetime


def misroute_nsl_info(row):
	# remove duplicate nsl in sequence and store count of misroute by dc
	nsl = []
	slid = {}
	for nc in row['_source']['nsl']:
		if not nsl:
			nsl.append(nc['code'])
			if nc['code'] in ['DTUP-207','DLYMR-118','DTUP-216']:
				slid[nc['slid']] = 1
		if nsl[-1] == nc['code']:
			continue
		else:
			nsl.append(nc['code'])
			if nc['code'] in ['DTUP-207','DLYMR-118','DTUP-216']:
				slid[nc['slid']] = slid.get(nc['slid'], 0) + 1

	return slid, sum(slid.values())


def get_distance(row, combo_map_dc):
	# get locality lat/lng
	loc_lat = row['_source'].get('aseg',{}).get('lat', 0.0)
	loc_lng = row['_source'].get('aseg',{}).get('lon', 0.0)

	# get mapped dc lat/lng
	map_lat, map_lng = dc_name_dict.get(combo_map_dc, [0.0, 0.0])
	# print map_lat, map_lng
	# get cn lat/lng
	cn_lat, cn_lng = dc_name_dict.get(row['_source'].get('cn',''), [0.0, 0.0])
	# print cn_lng, cn_lat #, loc_lat, loc_lng
	# loc found flag
	lf_flag = bool( row['_source'].get('aseg',{}).get('loc') )
	# print lf_flag
	# get loc2map distance
	if lf_flag and loc_lat and loc_lng and map_lat and map_lng:
		# loc2map_arial = aerial([loc_lat,loc_lng], [map_lat,map_lng])
		loc2map_arial = aerial(loc_lat,loc_lng,map_lat,map_lng)
		# print loc2map_arial
		loc2map_dist = aerial_to_road(loc2map_arial) if loc2map_arial else None
	else:
		loc2map_dist = None

	# get loc2cn distance 
	if lf_flag and loc_lat and loc_lng and cn_lat and cn_lng:
		# loc2cn_arial = aerial([loc_lat,loc_lng], [cn_lat,cn_lng])
		loc2cn_arial = aerial(loc_lat,loc_lng,cn_lat,cn_lng)
		# print loc2cn_arial
		loc2cn_dist = aerial_to_road(loc2cn_arial) if loc2cn_arial else None
	else:
		loc2cn_dist = None
	# print loc2map_dist, loc2cn_dist
	return loc2map_dist, loc2cn_dist


def get_live_flag(row):
	# if str(row['_source'].get('cpin', '')) in v3_pins:
	#     return 'v3'
	if str(row['_source'].get('cpin', '')) in pin_city.keys():
		return 'yes'
	else:
		return 'no'


def format_and_prepare(row):
	# get map dc
	cpin_map_dc, combo_map_dc, mapped_dc, recommended_dc, maximum_serving_dc, last_update_datetime = get_map_dc(row)
	# print "x"
	# get misroute nsl dc and count
	misroute_dc, misroute_count = misroute_nsl_info(row)
	# print "w"
	# get distance
	loc2map_dist, loc2cn_dist = get_distance(row, combo_map_dc)
	# print "v"
	# print loc2map_dist,loc2cn_dist


	# make dict of required data
	doc = {}
	doc['wbn'] = row['_id']
	doc['mwn'] = row['_source'].get('mwn', '')
	doc['add'] = row['_source'].get('add', '')
	doc['cpin'] = str(row['_source'].get('cpin', ''))
	doc['cty'] = row['_source'].get('cty', '')
	doc['aseg_loc'] = row['_source'].get('aseg',{}).get('loc', '')
	doc['aseg_pin'] = str(row['_source'].get('aseg',{}).get('pin', ''))[:6]
	doc['aseg_lat'] = row['_source'].get('aseg',{}).get('lat')
	doc['aseg_long'] = row['_source'].get('aseg',{}).get('lon')
	doc['cl'] = row['_source'].get('cl', '')
	doc['cn'] = row['_source'].get('cn', '')
	doc['cpin_map_dc'] = cpin_map_dc
	doc['combo_map_dc'] = combo_map_dc
	doc['mapped_dc'] = mapped_dc
	doc['recommended_dc'] = recommended_dc
	doc['maximum_serving_dc'] = maximum_serving_dc
	doc['misroute_nsl_count'] = misroute_count
	doc['misroute_nsl_dc'] = {dc_id_dict.get(k, k):v for k,v in misroute_dc.items() if k}
	doc['loc2map_dist'] = loc2map_dist
	doc['loc2cn_dist'] = loc2cn_dist
	doc['invalid_add_code'] = row['_source'].get('aseg',{}).get('invalid_add_code', '')
	doc['cnc'] = row['_source'].get('cnc', '')
	doc['date'] = str(date.today()-timedelta(1))
	doc['live'] = get_live_flag(row)
	doc['nsl_at_dc'] = any(m in dc_id_dict for m in misroute_dc)
	doc['region'] = row['_source'].get('rgn', '')
	doc['last_update_datetime'] = last_update_datetime
	doc['cd'] = row['_source'].get('cd', '')
	doc['cs_sd'] = row['_source'].get('cs',{}).get('sd', '')
	doc['pdd'] = row['_source'].get('pdd', '')
	doc['pdt'] = row['_source'].get('pdt', '')
	return doc


def misroute_main():
	# yesterday misroute data from elastic search
	es_misroute_data = get_misroute_data()
	
	# output coulumn values
	out_keys = ['wbn','mwn','add','cpin','cty','aseg_loc','aseg_pin','aseg_lat','aseg_long','cl','cn','cpin_map_dc','combo_map_dc','mapped_dc',\
	'recommended_dc','maximum_serving_dc','misroute_nsl_dc','loc2map_dist','loc2cn_dist',\
	'invalid_add_code','live','cnc','region','broad_cat','remark','team','last_update_datetime','cd','cs_sd','pdd']

	# output report file path
	out_file = path+'/files/misroute_rca_b2b_'+str(date.today())+'.csv'
	# out_file = path+'/files/misroute_rca_b2b_2019-03-14.csv'
	# out_file = path+'/files/misroute_rca_2018-09-20.csv'
	# output data
	out_data = []

	for i, data in enumerate(es_misroute_data):
		# print i
		#print("\rWorking on Record: [{}]".format(i), end=',')
		fdata = format_and_prepare(data)
		# print "z"
		broad_cat, remark, team = get_misroute_remark(fdata)
		# print "y"
		fdata['broad_cat'] = broad_cat
		fdata['remark'] = remark
		fdata['team'] = team
		# insert to mongo
		mis_col.insert_one(fdata)
		# csv keys doc
		fdata = {ok:fdata[ok] for ok in out_keys}
		out_data.append(fdata)

	print('data dumped into mongo.............completed')

	# write data to csv
	write_csv(out_data, out_keys, out_file)
	# os.remove(path+'/athena_data/misroute_rca_'+str(date.today())+'.csv')
if __name__ == '__main__':
	misroute_main()