from haversine import haversine

def aerial(g1, g2, g3, g4):
    # try:
    #     # haversine doesn't handle this constraint.
    #     if (-90 < g1[0] < 90) and \
    #             (-180 < g1[1] < 180) and \
    #             (-90 < g2[0] < 90) and \
    #             (-180 < g2[1] < 180):
    d = haversine((float(g1),float(g2)),(float(g3),float(g4)))
    # round to 2 decimal places
    return round(d, 2)
    # except Exception as e:
    #     return None


def aerial_to_road(aerial_distance):
    if 0 < aerial_distance <= 2000:
        aerial_multiplier_factor = 1.9346
    elif aerial_distance <= 5000:
        aerial_multiplier_factor = 1.5583
    elif aerial_distance <= 10000:
        aerial_multiplier_factor = 1.4458
    else:
        aerial_multiplier_factor = 1.4218
    d = aerial_distance * aerial_multiplier_factor
    return round(d, 2)