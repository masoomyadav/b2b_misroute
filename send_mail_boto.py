from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart

from os.path import basename
import boto.ses
import sys

AWS_REGION ='us-east-1'#'ap-southeast-1'

def mail_send(send_from, send_to, subject, text, files, htmltype=False):
	# via http://codeadict.wordpress.com/2010/02/11/send-e-mails-with-attachment-in-python/
	msg = MIMEMultipart()
	msg['Subject'] = subject
	msg['From'] = send_from
	msg['To'] = send_to
	# print(msg['To'])
	# what a recipient sees if they don't use an email reader
	msg.preamble = 'Multipart message.\n'
	
	# the message body
	# Record the MIME types of both parts - text/plain and text/html.
	if not htmltype:
		part = MIMEText(text, 'plain')
	else:
		part = MIMEText(text, 'html')
	# part = MIMEText(text)
	msg.attach(part)
	
	# the attachment
	for f in files:
		with open(f, "rb") as fil:
			msg.attach(MIMEApplication(fil.read(),
			                           Content_Disposition='attachment; filename="%s"' % basename(f),
			                           Name=basename(f)
			                           ))
	
	# connect to SES
	connection = boto.ses.connect_to_region(AWS_REGION)
	
	# and send the message
	rec = msg['To'].split(", ")
	print rec
	result = connection.send_raw_email(msg.as_string(), source=msg['From'], destinations=rec)
